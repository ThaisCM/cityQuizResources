package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.cityquiz.presentation.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void navigation(){
        onView(withId(R.id.btnStart))
                .perform(click());
        onView(withId(R.id.questionFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.btnOption1))
                .perform(click());
        onView(withId(R.id.btnOption2))
                .perform(click());
        onView(withId(R.id.btnOption3))
                .perform(click());
        onView(withId(R.id.btnOption4))
                .perform(click());
        onView(withId(R.id.btnOption5))
                .perform(click());
        onView(withId(R.id.scoreFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.btnPlayAgain))
                .perform(click());
        onView(withId(R.id.questionFragment))
                .check(matches(isDisplayed()));
    }
}
